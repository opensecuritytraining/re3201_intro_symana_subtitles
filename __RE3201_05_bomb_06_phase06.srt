﻿1
00:00:00,240 --> 00:00:04,906
So, we'll start in the last phase of
the binary bomb the phase six.

2
00:00:05,485 --> 00:00:11,040
And we'll do it, as always, they start
with the disassemble of the function.

3
00:00:12,560 --> 00:00:16,656
And the first interesting
thing scrolling down is that

4
00:00:17,656 --> 00:00:19,220
if there is a call for
the read_six_numbers,

5
00:00:19,245 --> 00:00:22,892
and this is a function
that we saw before.

6
00:00:22,917 --> 00:00:28,634
So, we can try to use the same
approach even without looking further,

7
00:00:28,659 --> 00:00:31,909
because it is the same function,
in the same binary,

8
00:00:32,200 --> 00:00:37,409
so, it probably works the
same way with this one.

9
00:00:37,936 --> 00:00:41,909
And if we scroll a little bit more,

10
00:00:41,921 --> 00:00:46,960
we found another similarity
with other phases.

11
00:00:47,520 --> 00:00:51,156
Here, we can see that
there is a structure.

12
00:00:52,140 --> 00:00:55,906
And this time, lldb says
it's a node.

13
00:00:56,924 --> 00:01:02,910
And for us, it just means
it's some kind of structure,

14
00:01:03,158 --> 00:01:05,412
And it's coming a little bit more.

15
00:01:07,440 --> 00:01:12,591
We can see another interesting
thing that is happening here,

16
00:01:12,616 --> 00:01:17,157
for example, we see that we have our
add one, and then compared to six,

17
00:01:17,904 --> 00:01:20,316
and if it's not equal,
it jumps from an address

18
00:01:20,341 --> 00:01:22,377
that is before the
address where we are.

19
00:01:22,402 --> 00:01:27,655
So again, this is a loop that is going to
run for six times before going further.

20
00:01:29,137 --> 00:01:35,409
The interesting part, though, is that
 it's jumping to the 0x18d8,

21
00:01:36,160 --> 00:01:39,157
and this is before the
node structure.

22
00:01:39,385 --> 00:01:42,584
So, it's iterating something
with the structure.

23
00:01:45,520 --> 00:01:48,160
We can go a little bit further,

24
00:01:48,852 --> 00:01:56,157
and then, we can see that here,
we have a call to the explode bomb.

25
00:01:56,658 --> 00:01:58,904
That's a call that we
don't want to take.

26
00:01:59,945 --> 00:02:01,658
and to skip this one,

27
00:02:02,640 --> 00:02:08,155
we want to find a jumper that is
going to jump after this address

28
00:02:08,408 --> 00:02:10,402
that comes before, right?

29
00:02:10,657 --> 00:02:17,656
And just right before, we can see that
there is this jump equal to 0x1961.

30
00:02:18,153 --> 00:02:23,410
That is right after and
this path lead us to the return.

31
00:02:24,327 --> 00:02:28,159
That is always our goal to
return and defuse the bomb.

32
00:02:28,481 --> 00:02:30,909
So now, that we have a
little bit of information,

33
00:02:31,330 --> 00:02:34,654
let's start with phase six.

34
00:02:35,760 --> 00:02:41,406
The first thing I want to do is check
the structure that we have there.

35
00:02:42,960 --> 00:02:45,154
So, we know how to do it now.

36
00:02:45,250 --> 00:02:47,656
We'll check with lldb.

37
00:02:49,139 --> 00:03:09,403
It's some kind of structure and
the address is 0x18e7, plus 0x3919.

38
00:03:09,652 --> 00:03:11,407
So, what's in there?

39
00:03:12,366 --> 00:03:20,154
Okay, we can see 12345 here,
and then some 1234,

40
00:03:20,880 --> 00:03:22,158
and this is one one.

41
00:03:22,545 --> 00:03:25,312
And here we have some kind of data.

42
00:03:25,337 --> 00:03:32,244
So, it could be some kind of actual
node of a linked list or something similar,

43
00:03:32,256 --> 00:03:37,653
but as you can see, it is
a structure of some sort.

44
00:03:38,155 --> 00:03:40,656
So, let's set up our addresses.

45
00:03:41,099 --> 00:03:47,409
The beginning of this space can
be our starting point, right?

46
00:03:47,654 --> 00:03:51,660
So, we'll just put like the first
address that we see here.

47
00:03:52,010 --> 00:03:57,904
That is 0x185b.

48
00:04:02,157 --> 00:04:07,407
And we are going to, again,
have two targets, one in the middle,

49
00:04:07,903 --> 00:04:13,909
so we can ease the work for angr because
this space is very memory hungry.

50
00:04:15,630 --> 00:04:19,407
And we also need the address
of the read_six_numbers,

51
00:04:20,200 --> 00:04:23,152
so that we can hook
like we did it before.

52
00:04:23,400 --> 00:04:27,905
So, let's find our first target.

53
00:04:35,040 --> 00:04:40,403
So, one of the interesting points
that we saw coming from here

54
00:04:40,991 --> 00:04:43,624
until the end of the phase,

55
00:04:44,800 --> 00:04:48,151
we saw that right in the middle,
you have the first branching point

56
00:04:48,200 --> 00:04:49,988
that is interesting for us,

57
00:04:50,240 --> 00:04:56,241
that it would be like finishing
this loop that is jumping like,

58
00:04:56,903 --> 00:04:59,157
that is like iterating six times.

59
00:04:59,653 --> 00:05:02,753
So, we know that this
loop is modifying

60
00:05:02,765 --> 00:05:08,857
or handling or manipulating the
structure that that we saw before.

61
00:05:08,882 --> 00:05:14,403
So, we want somehow to start
when the manipulation is over,

62
00:05:14,906 --> 00:05:17,904
because this is something
that needs to be done anyway.

63
00:05:18,374 --> 00:05:22,909
So, my first target would
be just after this jne,

64
00:05:23,405 --> 00:05:27,904
meaning that we finished the six iterations
that we want to have with the structure,

65
00:05:28,905 --> 00:05:31,159
and we are right after that.

66
00:05:31,401 --> 00:05:36,157
So, my first target
would be the 0x1906.

67
00:05:43,360 --> 00:05:44,655
So, and now for the second one.

68
00:05:44,965 --> 00:05:47,405
The second one is the one
that we already know.

69
00:05:47,520 --> 00:05:51,064
We want to reach the return here.

70
00:05:51,089 --> 00:05:55,660
As always, we want it before
we pop and destroy everything.

71
00:05:56,621 --> 00:06:02,903
So, whatever we do just,
we know that if it's not equal,

72
00:06:03,155 --> 00:06:05,907
we are jumping to this call.

73
00:06:06,655 --> 00:06:10,904
So, we want to make sure that
we are actually here, at least,

74
00:06:11,009 --> 00:06:15,103
and that's why we are going to
put our target as this one.

75
00:06:15,128 --> 00:06:19,409
So, before we destroy everything,
but after this jump,

76
00:06:19,654 --> 00:06:21,658
being sure that we
are going to return.

77
00:06:22,245 --> 00:06:26,654
So, this one is the 0x1971.

78
00:06:27,760 --> 00:06:30,906
And the read_six_numbers,
it's easy, we just go to the call.

79
00:06:33,800 --> 00:06:38,153
And it's 0x1c11, right?

80
00:06:38,960 --> 00:06:42,161
This is the address
of read_six_numbers.

81
00:06:45,520 --> 00:06:47,071
Okay, so there we go.

82
00:06:47,096 --> 00:06:50,344
We have our important
addresses for angr.

83
00:06:50,960 --> 00:06:55,405
We can copy-paste the
read_six_numbers function.

84
00:06:55,807 --> 00:06:59,155
I just changed it like to the six,
and then, a six.

85
00:07:00,032 --> 00:07:06,657
That's because we can, and then,
we need to do the hooking.

86
00:07:07,659 --> 00:07:13,408
As we saw before, we just need
to add a hook with project.hook,

87
00:07:13,656 --> 00:07:16,907
and then, we need to give it
like what we want to hook.

88
00:07:17,543 --> 00:07:19,905
That's the read_six_numbers address.

89
00:07:22,160 --> 00:07:26,654
What we what we want
to do with this hook.

90
00:07:27,196 --> 00:07:31,624
So, it's our read_six_numbers function.

91
00:07:36,400 --> 00:07:41,158
So, this state, it is already
multiple times by now.

92
00:07:41,881 --> 00:07:43,411
It's project factory,

93
00:07:45,157 --> 00:07:50,908
We'll start with a blank state that
starts in the beginning of the phases.

94
00:07:52,405 --> 00:07:56,152
This is the address

95
00:07:56,200 --> 00:08:01,655
This is phase six,

96
00:08:03,120 --> 00:08:06,154
and we'll create our simulation manager.

97
00:08:13,656 --> 00:08:18,395
By now, you already realize that
simulation manager or the simgr

98
00:08:18,906 --> 00:08:20,657
they are the same.

99
00:08:21,153 --> 00:08:27,153
So, we can just put our state in here,
and we are going to start on that.

100
00:08:27,655 --> 00:08:33,153
And this time, what we are going
to do is to find first the 6 a.

101
00:08:33,524 --> 00:08:37,304
So, what all we want is
to have active paths

102
00:08:37,920 --> 00:08:41,156
that lead us to finishing the loop.

103
00:08:41,400 --> 00:08:44,152
So, as it is the in-between state

104
00:08:44,949 --> 00:08:50,058
We'll just run it in a loop until
we found an activity state

105
00:08:50,083 --> 00:08:56,153
that is actually after the
iteration of the node structure.

106
00:08:58,000 --> 00:09:00,404
So, let's run it,

107
00:09:08,705 --> 00:09:13,206
and this space can take a while,
so, I will come back in a moment.

108
00:09:14,560 --> 00:09:16,056
Yay! It finished.

109
00:09:16,081 --> 00:09:22,702
And now, what we are going to do is also
similar to what we already did before,

110
00:09:23,120 --> 00:09:29,207
We are going to store the
found that is in founds,

111
00:09:29,704 --> 00:09:37,207
and then, we are going to initiate a
second symbolic execution from that point

112
00:09:37,704 --> 00:09:42,456
or that state of trying to
find the the end of the target

113
00:09:42,953 --> 00:09:47,172
like symbolic execution
between the two targets,

114
00:09:47,197 --> 00:09:53,703
the target one that we just found and the
target that is the end of the return.

115
00:09:54,541 --> 00:10:01,953
So, we'll run now the same,
and as soon as we find some path

116
00:10:02,000 --> 00:10:07,956
that is still active after
the iteration loop,

117
00:10:08,954 --> 00:10:13,949
and it goes, and it
finds the target_six_B

118
00:10:13,974 --> 00:10:20,454
that it was if we remember just
before destroying everything,

119
00:10:20,631 --> 00:10:27,454
but after that jump, that made
sure that we were going to return,

120
00:10:28,706 --> 00:10:33,705
then we are going to break and see
what's inside of the read_six_numbers.

121
00:10:34,500 --> 00:10:37,954
So, let's start this one.

122
00:10:38,455 --> 00:10:41,204
This shouldn't take that long.

123
00:11:04,000 --> 00:11:05,137
There you go.

124
00:11:06,320 --> 00:11:12,638
So, let's print the flag
to see what we have.

125
00:11:13,386 --> 00:11:16,888
And we have a sequence of six numbers.

126
00:11:17,136 --> 00:11:18,639
So, it could be right.

127
00:11:20,640 --> 00:11:26,887
We'll add this six numbers
into our bomb_flags.txt,

128
00:11:27,386 --> 00:11:33,387
and I'm going to run
it here in the shell,

129
00:11:33,635 --> 00:11:37,637
because the Python is
consuming a lot of memory,

130
00:11:38,160 --> 00:11:40,138
and there we go.

131
00:11:40,639 --> 00:11:44,199
We diffused all the six
phases of the binary bomb.


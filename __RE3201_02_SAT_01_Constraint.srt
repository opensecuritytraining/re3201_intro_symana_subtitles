﻿0:00:00.370,0:00:02.001
So, what is SAT?

0:00:02.026,0:00:10.260
SAT in logic and computer science is
called a Boolean satisfiability problem.

0:00:10.261,0:00:13.358
I like to call it a propositional
satisfiability problem,

0:00:13.383,0:00:17.400
because then I remember that SAT
is just about propositional logic.

0:00:17.401,0:00:22.471
But most of the times, you are going
to find it abbreviated as such,

0:00:22.496,0:00:30.154
since it's like the way the problems
are defined in complexity theory,

0:00:30.179,0:00:33.390
and where SAT comes from.

0:00:33.390,0:00:41.719
So, such problem is the problem of deciding
if that is a solution for a given problem

0:00:41.719,0:00:49.670
that satisfies this propositional
logic formula that we are evaluating.

0:00:49.670,0:00:53.293
So, in other words,
you have like a formula

0:00:53.318,0:00:57.739
that is written using
propositional logic ???

0:00:57.740,0:01:05.139
And we ask if there is a way to
have inputs into this formula

0:01:05.164,0:01:11.589
that it will evaluate to define
a prop from left to true.

0:01:11.614,0:01:19.870
the way we do it is
basically very inefficient.

0:01:19.871,0:01:23.353
And that's why the SAT
problem is the first problem

0:01:23.378,0:01:26.501
In fact, it was
proven to be ??? complete.

0:01:26.501,0:01:30.884
And NP complete just means
that someone ??? are better.

0:01:30.909,0:01:36.130
No one knows an algorithm that
can efficiently solve a problem.

0:01:36.130,0:01:41.840
So, it means we cannot solve this SAT
problem in an efficient way right now.

0:01:41.840,0:01:45.320
So, we don't know an algorithm for it.

0:01:45.345,0:01:50.030
About SMT, you can think
about it like an extra layer

0:01:50.042,0:01:58.826
that are added on top of offset
problem that adds to this whole tech,

0:01:58.851,0:02:04.850
a bit vectors, arithmetic,
support, and arrays.

0:02:04.850,0:02:10.989
So, if you think about SAT, we can just use
it with propositional logic ???.

0:02:10.989,0:02:18.856
With SMT solvers, we can also just use
close, minus, and multiply vectors.

0:02:18.881,0:02:21.801
It can even be a bit vectors.

0:02:21.801,0:02:30.791
And field arrays, ??? pretty similar way as
we have with normal programming language,

0:02:30.816,0:02:34.554
or more common programming language.

0:02:34.567,0:02:38.049
The SMT solvers, they allow us ???

0:02:38.074,0:02:44.019
??? for us to encode the same
problems in a more efficient way.

0:02:44.019,0:02:49.879
So, we don't need to translate our plus and
minus into ??? and ???

0:02:49.879,0:02:52.708
And in this way,
the solvers understand it better,

0:02:52.733,0:02:56.078
the structure of the
problem that we have.

0:02:56.103,0:03:03.823
If we can use the same functions and ideas
that we have from a more abstracted way,

0:03:03.848,0:03:07.093
then a simple propositional logic,

0:03:07.118,0:03:13.741
but SMT and SAT and of this kind
of SAT basic formal methods,

0:03:13.766,0:03:19.830
they rely a lot on the
concept of constraints.

0:03:19.830,0:03:21.802
So, what are constraints?

0:03:21.827,0:03:27.269
Constraints are just like limitations
on boundaries that you have.

0:03:27.269,0:03:28.915
It doesn't matter how.

0:03:28.940,0:03:31.159
It's just like a definition.

0:03:31.160,0:03:34.138
Eugene Freuder stated in 1997

0:03:34.163,0:03:40.845
that constraint programme is almost like
the holy grail of the programming concepts,

0:03:40.870,0:03:47.479
because until then, we we basically had
this kind of programming languages

0:03:47.504,0:03:50.387
that work it like writing a SAP,

0:03:50.399,0:03:54.900
So, we would write a programme
because we had a problem,

0:03:54.925,0:04:02.390
like I want a cake, and she'll have a cake
in the end of return of that function.

0:04:02.391,0:04:08.240
I needed to tell the programme,
like actually right now ???

0:04:08.265,0:04:11.371
that would bake that cake for me.

0:04:11.371,0:04:15.399
So, I needed to state how
many eggs and how much milk

0:04:15.424,0:04:21.353
and all this and in the sequence
they supposed to do each step,

0:04:21.378,0:04:25.211
so that it would return
the cake for me.

0:04:25.212,0:04:29.967
And the idea of constraint programming
is that you tell the computer

0:04:29.992,0:04:36.017
that you want a cake, and then,
you tell the computer what ??? kitchen

0:04:36.042,0:04:40.856
and the computer will tell you if it's
possible for you to bake the cake

0:04:40.868,0:04:44.336
with the stuff that you have
in your kitchen right now,

0:04:44.361,0:04:47.193
or if you need to do
some groceries before,

0:04:47.218,0:04:51.870
and if it's possible to bake the
cake within your kitchen right now.

0:04:51.871,0:04:56.775
It's going to give you all the solution,
also like the recipe,

0:04:56.800,0:05:00.451
and these kind of make
the computer ??? to me

0:05:00.476,0:05:03.716
what we expect it to be,
a problem solver.

0:05:03.741,0:05:07.224
We don't need to solve
the problem anymore.

0:05:07.249,0:05:12.776
We just need to have a problem
and give the limitations

0:05:12.801,0:05:15.470
that we have right now
are our constraints.

0:05:15.471,0:05:22.887
But the way that a SAT works with these
constraints are not very efficient.

0:05:22.912,0:05:26.793
We are going to do it with pen and paper

0:05:26.818,0:05:33.040
just to have an idea how most of
the SAT solving algorithm works.

0:05:33.041,0:05:37.801
And for doing that, we start with
this propositional logic equation.

0:05:37.801,0:05:43.991
And we are going to solve with the
same steps that are ???

0:05:43.991,0:05:45.866
So, we consider this,

0:05:45.891,0:05:51.490
and the first thing that we need to do
is evaluate word which were our inputs.

0:05:51.491,0:05:54.811
And we have a, b, and c in this case.

0:05:54.811,0:05:57.055
So what we'll do is build a table,

0:05:57.080,0:06:04.341
and we'll write all the possible
permutations that we have for a, b, and c.

0:06:04.366,0:06:08.449
Zero being like false,
and one being true.

0:06:08.450,0:06:16.308
The next step is, at least the way I think
it's easier to solve these kind of problems is

0:06:16.333,0:06:19.189
breaking it into smaller pieces.

0:06:19.190,0:06:27.831
Now I know I defined a ??? possible
input and permutations of it.

0:06:27.831,0:06:32.668
And then what I'll do is, I get the
first part of the equation in NP,

0:06:32.693,0:06:37.857
and I put it in a column,
and then, I check the next one,

0:06:37.882,0:06:41.240
and it's ! B, it's the next column.

0:06:41.241,0:06:46.291
And then I need to solve  b, !b, and c, 
and it's the next column.

0:06:46.291,0:06:52.780
And then I have the whole equation that is
going to be the answer for my truth table.

0:06:52.780,0:06:58.250
That's the name of this table
that we are building right now.

0:06:58.250,0:07:02.770
So, as we define it,
we can start solving it.

0:07:02.770,0:07:06.211
So, the first one is an N connector,

0:07:06.236,0:07:12.204
which means that we need to have a
and b valid to have a valid a and b.

0:07:12.229,0:07:14.260
So, what happens here is,

0:07:14.285,0:07:18.718
for example, if we have a
is false, and b is false,

0:07:18.743,0:07:21.798
and in b are also going to be false.

0:07:21.823,0:07:28.611
The same is valid for 'a is false' and 'b
is true,' because just one of it is true.

0:07:28.612,0:07:34.332
And so as you can imagine, we have the same
for a is through and b is false.

0:07:34.332,0:07:42.832
And we are just going to have it
set to true if a and b are true.

0:07:42.832,0:07:45.391
So, that's pretty easy to remember.

0:07:45.391,0:07:48.223
The next one was !b.

0:07:48.248,0:07:54.188
So, !b is also not
that hard to remember,

0:07:54.213,0:07:58.947
because we just need to invert
the value that we have in b.

0:07:58.972,0:08:04.360
So, if b is false, not
false, means it is true.

0:08:04.360,0:08:07.060
And that's what we have here.

0:08:07.060,0:08:13.069
And if we have b set to true,
not true is going to be false.

0:08:13.069,0:08:22.039
And that's what we do for all our truth
table or other cases that we have here.

0:08:22.039,0:08:24.740
So, the next one is !b and c,

0:08:24.765,0:08:30.579
So, we are going to get the
!b that we just reasoned about,

0:08:30.604,0:08:35.190
and then, we get the c
from the first column.

0:08:35.191,0:08:39.029
So, if you have !b true and c is false,

0:08:39.040,0:08:43.590
we are having false, because
it's also an end connector.

0:08:43.590,0:08:48.560
And then, if you have !b is true,
and c is true.

0:08:48.560,0:08:54.090
So ,we have true and true
is going to be true.

0:08:54.090,0:08:59.450
And then, we have the last one.

0:08:59.450,0:09:06.920
So, our last one is the whole equation
that has in the middle, the 'or.'

0:09:06.945,0:09:13.440
The or is going to work,
 also not that hard.

0:09:13.440,0:09:16.801
We know that in the first part of it,
we have false,

0:09:16.826,0:09:18.450
the second part is false.

0:09:18.450,0:09:22.050
So, we have false or false
is going to be false.

0:09:22.050,0:09:26.703
But ??? is that we have false or true,

0:09:26.728,0:09:31.510
it's going to be true,
because we just need one.

0:09:31.510,0:09:36.420
And we go through the whole table.

0:09:36.420,0:09:39.240
So, now that we have solved it,

0:09:39.240,0:09:46.404
The question here for the exercise of the
training is reconsider the same equation

0:09:46.429,0:09:49.389
that we have right
now on display.

0:09:49.389,0:09:54.188
And we said a, b and c,
all of them to true.

0:09:54.213,0:09:57.477
What's the answer that we
have for the truth table?
﻿1
00:00:00,229 --> 00:00:04,929
So when you talk about the SAT
solver or the SAT problem,

2
00:00:04,954 --> 00:00:07,148
it means that, given a formula,

3
00:00:07,173 --> 00:00:10,829
it needs to be on propositional
logic for the SAT.

4
00:00:10,829 --> 00:00:13,149
It doesn't need to be
propositional logic.

5
00:00:13,174 --> 00:00:19,393
It can use arithmetic and bit factor and
all this in the case of SMT solvers,

6
00:00:19,418 --> 00:00:24,684
but given this problem, it is going to
decide if it's feasible or not to solve it.

7
00:00:24,709 --> 00:00:30,155
The important thing to remember about
the solutions that come from the SAT

8
00:00:30,180 --> 00:00:35,720
or the SMT solvers is that they
need to be SOUND and COMPLETE.

9
00:00:35,720 --> 00:00:37,410
And what does it mean?

10
00:00:37,410 --> 00:00:42,103
So, SOUND means that the algorithm used to
find the results

11
00:00:42,128 --> 00:00:45,430
are not going to find untrue results.

12
00:00:45,431 --> 00:00:51,191
And COMPLETE means that the algorithm
is going to check all possible inputs,

13
00:00:51,215 --> 00:00:55,340
all permutations,
like we did in the truth table.

14
00:00:55,340 --> 00:00:59,596
So, this means that if this SAT
problem gives you a solution

15
00:00:59,621 --> 00:01:01,550
that is sound and complete,

16
00:01:01,550 --> 00:01:05,960
it means that it will work on all possible
inputs like we did in a truth table,

17
00:01:05,985 --> 00:01:10,020
and got the right answer all the time.

18
00:01:10,021 --> 00:01:16,807
So, this has been used for our
automated test case generation,

19
00:01:16,832 --> 00:01:24,590
like for fuzzing, for formal verification
of hardware and software, and so on.

20
00:01:24,590 --> 00:01:31,614
So, the whole idea of using SMT solvers
for security is to create a model

21
00:01:31,639 --> 00:01:35,621
that reasons about security
properties that we want to check.

22
00:01:35,621 --> 00:01:43,850
The SMT solvers are very strong also on the
langsec community that uses the SMT solvers

23
00:01:43,851 --> 00:01:47,968
for formal verification of
the language specification

24
00:01:47,979 --> 00:01:51,984
like building restrictions inside
the parser and the compilers,

25
00:01:52,009 --> 00:01:56,265
so that they are sure that in the end,

26
00:01:56,290 --> 00:02:02,050
the binary is secure via the
language specification is secure.

27
00:02:02,050 --> 00:02:06,970
So, you can see SMT solvers like applying
mathematics to computer science.


﻿1
00:00:00,290 --> 00:00:05,044
Hello and welcome to reverse
engineering 3201, symbolic analysis.

2
00:00:05,069 --> 00:00:10,616
Symbolic execution and symbolic
analysis are very powerful tools

3
00:00:10,641 --> 00:00:16,800
for bug hunting, code verification,
and reverse engineering tasks overall.

4
00:00:16,800 --> 00:00:20,150
In this class, we are going
to dive into concepts

5
00:00:20,175 --> 00:00:22,830
like SMT solver constraint programming,

6
00:00:22,830 --> 00:00:33,621
but we are also going to see how binary
analysis frameworks are using SMT solver

7
00:00:33,646 --> 00:00:38,036
and constraint programming
in our backend

8
00:00:38,061 --> 00:00:44,640
to facilitate the use of these
techniques for binary analysis.

9
00:00:44,641 --> 00:00:46,511
It's going to be a very practical course.

10
00:00:46,511 --> 00:00:49,658
So, we are going to solve
a lot of CTF challenges on the way,

11
00:00:49,669 --> 00:00:52,570
Of course, you could solve them,

12
00:00:52,571 --> 00:00:56,361
sometimes, easily with other reverse
engineering techniques,

13
00:00:56,361 --> 00:00:58,595
but that's not the case in this course.

14
00:00:58,620 --> 00:01:05,111
We are going to use
angr and all its features for it.

15
00:01:05,111 --> 00:01:08,285
So, the requirements for this course are python,

16
00:01:08,296 --> 00:01:11,834
So, I'm not going into the
details of the Python code.

17
00:01:11,859 --> 00:01:14,729
You're supposed to see it
and understand it.

18
00:01:14,729 --> 00:01:16,971
If you're not familiar
with Python yet,

19
00:01:16,996 --> 00:01:21,321
please, go ahead and
visit learnpython.org.

20
00:01:21,321 --> 00:01:23,572
And the same is valid for assembly.

21
00:01:23,597 --> 00:01:25,631
and X86_64 binaries.

22
00:01:25,631 --> 00:01:27,760
So, if you are also
not that comfortable,

23
00:01:27,785 --> 00:01:29,699
or just want a refresh,

24
00:01:29,724 --> 00:01:37,580
feel free to go ahead in OST2
and redo the reverse engineering path.

25
00:01:37,580 --> 00:01:45,110
Some of the goals of this course is to
understand how SMT solver works in general.

26
00:01:45,110 --> 00:01:48,476
And then, we are going to
shift into symbolic execution

27
00:01:48,501 --> 00:01:52,230
and the techniques that are behind
it, the algorithm and this stuff,

28
00:01:52,255 --> 00:01:56,970
and we are going to get all the flags.

29
00:01:56,970 --> 00:01:58,650
A short disclaimer.

30
00:01:58,650 --> 00:02:00,840
This course is a personal thing,

31
00:02:00,840 --> 00:02:06,840
personal project,
 nothing to do with my employee.

32
00:02:06,840 --> 00:02:13,652
And about the agenda,
we are going to have a basic intro.

33
00:02:13,677 --> 00:02:15,611
I will talk a little bit about logic.

34
00:02:15,636 --> 00:02:19,438
We are going to see the
symbolic execution algorithm.

35
00:02:19,463 --> 00:02:22,671
We are going to play a little bit
with z3 to get a feeling about it.

36
00:02:22,671 --> 00:02:28,640
Then, we go to angr and we solve
more complex challenges with it

37
00:02:28,665 --> 00:02:32,471
and in the end, we are going
to use all the techniques

38
00:02:32,496 --> 00:02:36,185
that we learned throughout the
course to solve the binary bomb

39
00:02:36,210 --> 00:02:41,151
that you should be familiar with already.

40
00:02:41,151 --> 00:02:42,480
And that's me.
See you


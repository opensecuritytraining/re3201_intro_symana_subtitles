﻿1
00:00:00,170 --> 00:00:05,270
So, a classical example of using
constraints and logical programming

2
00:00:05,295 --> 00:00:10,519
and SMT solvers is normally
solving small games.

3
00:00:10,519 --> 00:00:17,160
And as a homework, I'm not
going to solve it for you,

4
00:00:17,160 --> 00:00:21,203
but it's an extra exercise.

5
00:00:21,228 --> 00:00:27,830
There is the skeleton for
solving Sudoku board.

6
00:00:27,830 --> 00:00:36,180
You have the code like the board,
and there's a skeleton on your VM.

7
00:00:36,180 --> 00:00:44,815
And the order is already telling you
which kind of good libraries you have

8
00:00:44,840 --> 00:00:47,702
that you can use for your sudoku.

9
00:00:47,715 --> 00:00:53,999
You already have the puzzle file linked
that you would like to parse to solve,

10
00:00:53,999 --> 00:00:57,399
and it's all there.

11
00:00:57,399 --> 00:01:02,702
The only thing that I would like
to give a hint about is Distinct,

12
00:01:02,727 --> 00:01:19,530
and Distinct is a very nice function
to look for the documentation here.

13
00:01:19,530 --> 00:01:28,140
As you can see, the distinct is always
trying to find something that is simplified.

14
00:01:28,140 --> 00:01:30,563
So, you know that you are
looking for something

15
00:01:30,588 --> 00:01:34,211
that is distinct from everything else.

16
00:01:34,211 --> 00:01:38,902
So, if you have like, a Sudoku
board with numbers in a row,

17
00:01:38,927 --> 00:01:45,119
that can be from one to nine, but they
should be different from each other.

18
00:01:45,120 --> 00:01:51,378
Then, it means that you want to put
distinct numbers in a row from one to nine,

19
00:01:51,403 --> 00:01:53,150
if that makes sense.

20
00:01:53,151 --> 00:01:54,151
So, that's the hint.

21
00:01:54,151 --> 00:02:01,594
Look into how you can
use it for your sudoku

22
00:02:01,619 --> 00:02:08,980
and upload your solution or open-source it
and send us the link so we can discuss it.


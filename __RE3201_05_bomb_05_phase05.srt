﻿1
00:00:00,320 --> 00:00:04,059
We are finally in the phase five.

2
00:00:05,158 --> 00:00:08,644
So, we are almost done with the bomb.

3
00:00:08,669 --> 00:00:12,905
We are going to start
like any other phase.

4
00:00:13,654 --> 00:00:17,384
We have to disassemble of it to
understand what's happening,

5
00:00:17,920 --> 00:00:21,155
at least a little bit too
easy the work from angr.

6
00:00:21,220 --> 00:00:27,316
And just looking into it,
it's a very short phase,

7
00:00:27,341 --> 00:00:31,191
but it has something
different than the others.

8
00:00:31,216 --> 00:00:34,480
You can see here that lldb believes that

9
00:00:34,480 --> 00:00:36,511
there is an array in here.

10
00:00:37,656 --> 00:00:40,590
We don't need to take it by the word
but .

11
00:00:40,615 --> 00:00:44,152
But we can also assume that
it's some kind of a structure,

12
00:00:44,200 --> 00:00:51,654
because if it wasn't, our lldb
wouldn't recognise it somehow.

13
00:00:52,560 --> 00:00:57,654
So, here is scanf call pretty
similar to other phases.

14
00:00:57,893 --> 00:01:06,132
And we are going to check which kind
of input these level is expecting.

15
00:01:07,408 --> 00:01:09,704
So, we are going to do the same

16
00:01:10,880 --> 00:01:15,906
as in the other levels to
this using lldb for it.

17
00:01:16,880 --> 00:01:27,405
And the address we
need is 0x17eb, and 0x1b24.

18
00:01:29,911 --> 00:01:31,263
So, let's check.

19
00:01:32,406 --> 00:01:34,303
So, it's interesting that this level,

20
00:01:34,328 --> 00:01:37,905
or this phase is also
expecting two digits,

21
00:01:39,680 --> 00:01:44,120
which means it may might
be similar to phase three.

22
00:01:44,145 --> 00:01:47,304
We can try to solve it the same way,

23
00:01:47,840 --> 00:01:51,904
even though we see
this structure here.

24
00:01:52,156 --> 00:01:54,905
But we can also then have
a look at the structure

25
00:01:58,480 --> 00:02:00,657
and try to understand
a little bit more.

26
00:02:02,967 --> 00:02:05,902
So, we have no idea what
it has seen there, right?

27
00:02:14,240 --> 00:02:23,404
And the address is 0x1814 offset 0x19ac.

28
00:02:27,280 --> 00:02:32,433
And, as you can see, it is
some kind of a structure,

29
00:02:32,458 --> 00:02:38,624
because it's starting with 0
and going over until 0xf,

30
00:02:38,649 --> 00:02:43,101
and we can see that it's not in order.

31
00:02:44,156 --> 00:02:48,855
So, maybe linked list or some similar
structure into there.

32
00:02:50,155 --> 00:02:52,908
Looking a little bit
more into our binary,

33
00:02:53,740 --> 00:02:58,667
we can see that it compares eax to 0xf

34
00:02:59,655 --> 00:03:04,405
and if it's equal it jumps to 0x1835.

35
00:03:06,154 --> 00:03:10,407
0x1835 is a bad jump because
it goes to explode bomb.

36
00:03:10,905 --> 00:03:17,410
So, we already know that we don't want it
going a bit further into the structure,

37
00:03:17,800 --> 00:03:19,810
what's happening here?

38
00:03:19,835 --> 00:03:25,658
We can see that there is a
jump not equal to 0x1814,

39
00:03:26,405 --> 00:03:30,168
and 0x1814 is an address
before these lines.

40
00:03:30,193 --> 00:03:35,920
So, it's kind of a loop that
it jumps as long as not equal.

41
00:03:38,800 --> 00:03:41,407
eax is not equal to 15,

42
00:03:41,850 --> 00:03:51,656
and eax is being set in here, and
this is a value from our structure.

43
00:03:52,407 --> 00:03:56,409
As we know, there is
this 15 Just in here,

44
00:03:57,272 --> 00:03:59,156
in 0xf

45
00:03:59,906 --> 00:04:05,159
and we can assume that as
soon as we reach the 0xf,

46
00:04:06,369 --> 00:04:14,068
we are going to break from the
loop and move forward interesting,

47
00:04:14,093 --> 00:04:19,349
It's also that we have
this compare edx with 0xf.

48
00:04:19,374 --> 00:04:24,364
Edx is a counter that is starting
from zero and adding one,

49
00:04:24,389 --> 00:04:29,360
so, which means that our
loop needs to run 15 times

50
00:04:30,080 --> 00:04:35,457
until it gets to the 0xf,
because if it doesn't,

51
00:04:35,469 --> 00:04:38,807
it also jumps to the explode bomb.

52
00:04:38,832 --> 00:04:47,158
If it is equal, it is going
to compare something to ecx

53
00:04:47,403 --> 00:04:55,652
that adding the values from
eax to another address

54
00:04:55,907 --> 00:04:58,405
that is 0x183a

55
00:04:59,406 --> 00:05:01,652
That is another value
that is in the stack,

56
00:05:01,677 --> 00:05:03,904
so, that's our input.

57
00:05:04,504 --> 00:05:11,908
And if it's equal, it jumps to 0x183a.

58
00:05:13,158 --> 00:05:15,650
And this is just skipping
the explode bomb.

59
00:05:15,675 --> 00:05:21,505
So, we really want to give
the right sum or addition

60
00:05:21,517 --> 00:05:25,059
that is going to come out from this.

61
00:05:25,084 --> 00:05:32,154
Loop there, it's walking
from here until 0xf,

62
00:05:32,906 --> 00:05:38,659
because then, we are just
going to go down here.

63
00:05:39,155 --> 00:05:43,908
And if it's not equal,
or it calls something,

64
00:05:44,153 --> 00:05:46,032
but we want the return.

65
00:05:46,057 --> 00:05:50,902
So, what we are going to do is try
to solve it similar to phase three.

66
00:05:51,658 --> 00:05:54,134
First, we are going to
start just after a scanf.

67
00:05:54,159 --> 00:05:56,906
So, a scanf was here.

68
00:05:57,542 --> 00:06:01,409
We are going to start here, 0x17f0.

69
00:06:06,654 --> 00:06:15,165
And our target is going to be the return,
but before it destroys our stack.

70
00:06:15,408 --> 00:06:19,908
So, just one instruction,
before it's the 0x1848.

71
00:06:33,280 --> 00:06:38,357
And then, we are going to run a
simulation with these values

72
00:06:38,382 --> 00:06:40,408
just similar to phase three.

73
00:06:40,719 --> 00:06:46,653
So first locate our state that is a
blank state with the start address

74
00:06:47,153 --> 00:06:48,407
of after you scanf.

75
00:06:49,157 --> 00:06:57,840
Then, we push into our stack symbolic
values that are going to be our fake input.

76
00:07:00,720 --> 00:07:07,406
And for this we can use again
claripy bitvector symbolic,

77
00:07:08,720 --> 00:07:20,910
and create some symbolic
values that are just like this.

78
00:07:20,950 --> 00:07:23,903
with the 64 bit.

79
00:07:24,906 --> 00:07:26,154
Then, we'll create a simulation

80
00:07:26,204 --> 00:07:32,154
that by now, you all know
how to do it like this,

81
00:07:32,406 --> 00:07:38,406
and with base is start state or created state
with our symbolic values in the stack.

82
00:07:39,360 --> 00:07:42,405
And then, we'll create
the exploration.

83
00:07:43,318 --> 00:07:48,150
And we use as a find
parameter is our target.

84
00:07:48,175 --> 00:07:53,655
As always, we are trying
to avoid the bomb,

85
00:07:54,547 --> 00:07:56,908
and we'll enable veritesting.

86
00:08:02,960 --> 00:08:09,653
So, angr found one state
that goes into our target.

87
00:08:09,775 --> 00:08:17,053
We'll pop that value into a temporary
variable so that we don't lose it.

88
00:08:17,078 --> 00:08:25,659
And what we are going to do here is just
get the value from the high part of it,

89
00:08:26,157 --> 00:08:29,537
because that's the
value that we still don't know

90
00:08:30,691 --> 00:08:34,378
The ecx in the end, right?

91
00:08:34,936 --> 00:08:42,689
From for the first one, we know it already,
it's five from reversing it.

92
00:08:43,455 --> 00:08:53,164
So, we can print it, and we can see the
value for the second input would be 115.

93
00:08:53,189 --> 00:09:02,640
We'll push that into our flags file,
and then, I'm going to test it in here.

94
00:09:04,960 --> 00:09:08,884
And as you can see,
we diffused 1, 2, 3, 4, 5.

95
00:09:08,909 --> 00:09:13,864
We just solved the  phase
five on the binary bomb.


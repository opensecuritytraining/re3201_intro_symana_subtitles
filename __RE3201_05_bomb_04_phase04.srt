﻿1
00:00:00,080 --> 00:00:02,067
Let's start with phase four.

2
00:00:02,092 --> 00:00:05,772
As always the first thing
we do is disassemble it

3
00:00:05,797 --> 00:00:09,902
and get an idea of what's
happening in binary.

4
00:00:11,541 --> 00:00:13,487
And phase four.

5
00:00:15,360 --> 00:00:21,160
This call to scanf that is getting
our input and parsing it.

6
00:00:21,905 --> 00:00:24,674
And this time, we know
we need two inputs,

7
00:00:24,699 --> 00:00:29,489
if not, we're jumping to 0x1782.

8
00:00:29,514 --> 00:00:32,664
That is our explode bomb.

9
00:00:33,680 --> 00:00:35,914
So, we do not want that.

10
00:00:35,939 --> 00:00:40,504
We want to actually
skip the explode bomb.

11
00:00:42,160 --> 00:00:48,153
So, this means that we want
to come to this line here

12
00:00:48,155 --> 00:00:50,655
and see that everything is fine.

13
00:00:50,680 --> 00:00:58,450
We have 14 and end up
just right to the spot here.

14
00:00:58,475 --> 00:01:01,412
If you look down again,

15
00:01:01,659 --> 00:01:03,407
we have another call to explode bomb

16
00:01:03,410 --> 00:01:11,905
that can be skipped one instruction
before with the jump up to 0x17aa.

17
00:01:12,911 --> 00:01:18,080
That is just destroying the stack and
returning exactly the way we wanted to.

18
00:01:19,280 --> 00:01:21,908
So, let's check what's
happening in this,

19
00:01:21,933 --> 00:01:26,240
because we have a call now
to another function func4.

20
00:01:27,360 --> 00:01:31,840
If you look before func4, we can see
that it's taking three parameters,

21
00:01:32,656 --> 00:01:35,108
some kind of input
that is in our stack.

22
00:01:35,120 --> 00:01:37,080
So probably, our input,

23
00:01:37,105 --> 00:01:41,681
and then, we have a second
parameter that is zero,

24
00:01:41,706 --> 00:01:45,908
and third parameter that is 14.

25
00:01:46,655 --> 00:01:49,864
It's interesting to
see that this compare

26
00:01:50,400 --> 00:01:57,405
that goes to explode bomb says that
the func4 should return 10 or 0xa.

27
00:01:58,903 --> 00:02:00,912
Let's follow what func4 is doing.

28
00:02:03,406 --> 00:02:09,904
So, lldb command, and we want to disassemble func4.

29
00:02:22,408 --> 00:02:30,405
Func4 is taking our variable like the 14

30
00:02:30,655 --> 00:02:35,668
and doing stuff with it,
and comparing with edi

31
00:02:35,693 --> 00:02:41,232
that has not been changed until now.

32
00:02:41,657 --> 00:02:47,154
We know that ebx is being added
to esi and esi was zero.

33
00:02:47,404 --> 00:02:51,224
So, it was initializing,
probably to do the operations.

34
00:02:53,360 --> 00:03:00,160
If everything here is greater,
it's going to jump here,

35
00:03:00,185 --> 00:03:07,407
and as you can see here, it's like an index
walk back to the beginning of the function.

36
00:03:07,655 --> 00:03:10,282
So, it's kind of a recursive function,

37
00:03:10,307 --> 00:03:14,739
and if it less, it's jumping to 0x3f.

38
00:03:14,764 --> 00:03:19,909
0x3f is like walking index.

39
00:03:20,156 --> 00:03:24,326
It also jumps to the
beginning of func4.

40
00:03:25,904 --> 00:03:29,920
The differences the index is edx or esi.

41
00:03:30,906 --> 00:03:34,125
When it's equal, edi
and ebx are equal.

42
00:03:34,150 --> 00:03:39,908
We are going to store
ebx in eax and return,

43
00:03:40,404 --> 00:03:44,751
and we know that this eax
is supposed to be 10,

44
00:03:45,903 --> 00:03:49,768
because that's the value
that we want to have

45
00:03:49,793 --> 00:03:53,656
when we return to phase four,

46
00:03:53,906 --> 00:03:59,903
so that we'll skip the explode bomb and
actually return to the main function.

47
00:04:01,280 --> 00:04:07,116
If that's not clear to you,
have a look at it again

48
00:04:07,141 --> 00:04:12,500
and maybe check your
notes on reversing 101.

49
00:04:13,903 --> 00:04:18,649
We're going to start our
angr process again.

50
00:04:18,674 --> 00:04:27,227
So, the beginning of phase four can
be the beginning of func4 for us,

51
00:04:27,252 --> 00:04:30,153
because we want to skip
the first bomb explode,

52
00:04:30,659 --> 00:04:33,322
and that doesn't really
change anything,

53
00:04:33,347 --> 00:04:34,310
the flag,

54
00:04:34,335 --> 00:04:37,408
everything that is important for
the flag is happening here anyway.

55
00:04:38,406 --> 00:04:45,904
So, we can set our phase four
at the beginning of func4,

56
00:04:46,904 --> 00:04:48,904
and the same is valid for the targets.

57
00:04:49,200 --> 00:04:53,909
If return from func4.

58
00:04:53,934 --> 00:04:59,335
We are having the flag for phase four.

59
00:04:59,359 --> 00:05:02,128
So, it is 0x1732.

60
00:05:06,240 --> 00:05:12,902
Now that we have our address,
we can create our flag state.

61
00:05:14,080 --> 00:05:16,400
as we have been doing before.

62
00:05:16,425 --> 00:05:24,902
We need to create a symbolic flag
or the state to start with func4.

63
00:05:25,651 --> 00:05:28,664
As we remember from the binary,

64
00:05:30,400 --> 00:05:38,389
we need these three parameters
that are being passed to func4.

65
00:05:38,414 --> 00:05:41,906
So, we know that edi is our input.

66
00:05:43,652 --> 00:05:47,403
That should be symbolic because
that's what we want to do,

67
00:05:47,903 --> 00:05:49,144
to find out what it is.

68
00:05:50,080 --> 00:05:54,580
We have esi that is zero
and edx that is 14.

69
00:05:54,605 --> 00:05:59,909
So, let's set these
values in our state.

70
00:06:01,768 --> 00:06:03,657
So, edx is 14.

71
00:06:07,371 --> 00:06:09,252
esi was zero,

72
00:06:09,277 --> 00:06:16,161
and our edi is supposed
to be our input, right?

73
00:06:17,408 --> 00:06:22,712
And this input is our
partial flag, if you will.

74
00:06:22,737 --> 00:06:25,956
And this is something
that we don't know.

75
00:06:25,981 --> 00:06:29,840
So, we are going to create a
bit factor that is symbolic,

76
00:06:31,440 --> 00:06:35,405
and it is going to be my input.

77
00:06:36,407 --> 00:06:42,941
As it is in edi, we can assume
that it's a 32-bit vector.

78
00:06:43,911 --> 00:06:52,689
And we'll run our func4
now with these values,

79
00:06:52,714 --> 00:06:54,339
and let's check.

80
00:06:55,406 --> 00:06:56,506
It found one state.

81
00:06:56,531 --> 00:07:01,430
So, let's check what's in that state.

82
00:07:01,455 --> 00:07:09,704
As we remember, we saw here
that this add ebx to esi,

83
00:07:10,480 --> 00:07:14,653
and ebx is supposed to
be compared to edi,

84
00:07:15,658 --> 00:07:17,409
and this is going to return 10.

85
00:07:18,655 --> 00:07:26,960
So, we can assume that what we need
to input is 10 minus our input,

86
00:07:28,240 --> 00:07:30,403
and that's what we'll do here.

87
00:07:30,500 --> 00:07:40,089
we'll get our input that is the symbolic
value from the state that angr found

88
00:07:40,114 --> 00:07:43,134
to lead us to the target.

89
00:07:43,159 --> 00:07:46,409
And then, we'll make
like 10 minus this value.

90
00:07:47,180 --> 00:07:50,906
So, it's ten minus my input.

91
00:07:52,320 --> 00:07:56,776
And this is supposed to be
our flag, our partial flag.

92
00:07:56,801 --> 00:07:58,907
So, this is going to be three.

93
00:07:59,000 --> 00:08:04,166
So, we'll get the value three
and the string 10

94
00:08:04,191 --> 00:08:09,902
and we add them to our flag file,

95
00:08:11,156 --> 00:08:11,910
and then we'll run,

96
00:08:16,154 --> 00:08:17,423
and there you go.

97
00:08:17,448 --> 00:08:19,089
We got that one too.


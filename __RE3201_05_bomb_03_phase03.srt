1
00:00:01,110 --> 00:00:02,906
Let's start with phase three.

2
00:00:03,652 --> 00:00:07,769
As always, we have to disassemble to check how
Phase two,

3
00:00:07,794 --> 00:00:10,839
just to remind us how it looks like.

4
00:00:12,587 --> 00:00:16,191
We know from other tools that
this is a call to scanf.

5
00:00:16,216 --> 00:00:20,379
You can check it in
with gdb if you want,

6
00:00:20,404 --> 00:00:23,885
and then, we can see the first jump.

7
00:00:23,910 --> 00:00:30,334
We are trying to create like a
map of the function right now.

8
00:00:31,089 --> 00:00:34,690
And that's why we are looking
for jumps and compare.

9
00:00:34,715 --> 00:00:36,874
So, branching points.

10
00:00:36,899 --> 00:00:41,088
The first jump is this one,
so, we just compare.

11
00:00:41,838 --> 00:00:45,341
And eax with one is less or equal.

12
00:00:45,839 --> 00:00:51,340
We're jumping to x1688, and this
is a call for explode bomb,

13
00:00:51,400 --> 00:00:54,231
This is a bad jump

14
00:00:54,256 --> 00:00:57,840
If we scroll a little bit more,

15
00:00:58,401 --> 00:01:12,838
we can see the next interesting jump
is just like here it is a jmp equal to 0x16be.

16
00:01:13,585 --> 00:01:15,710
And if you look at the addresses,

17
00:01:15,735 --> 00:01:20,339
you can see that it's basically just
skipping the explode bomb function call

18
00:01:20,410 --> 00:01:27,836
and continuing all the way to the return,
as in the other phases.

19
00:01:28,338 --> 00:01:31,305
We want to reach the return,
because when we return,

20
00:01:31,330 --> 00:01:35,402
we defuse the bomb,
meaning we have the flag

21
00:01:35,427 --> 00:01:39,397
So, let's have a look
again at the beginning.

22
00:01:39,422 --> 00:01:45,244
We know that this is a
call to scanf.

23
00:01:45,269 --> 00:01:49,836
Let's have a look at the parameter
that is been passed to it.

24
00:01:50,105 --> 00:01:56,836
That would be the 0x1660, plux 0x1caf.

25
00:01:58,489 --> 00:02:08,585
So, have a look and it's
expecting two digits.

26
00:02:10,007 --> 00:02:12,339
So, let's start angr now.

27
00:02:14,144 --> 00:02:25,662
The base address for phase three
can be just after the call to scanf

28
00:02:31,336 --> 00:02:37,323
because as we know what their
function is expecting right now,

29
00:02:37,348 --> 00:02:38,839
meaning two digits.

30
00:02:39,400 --> 00:02:49,587
We can restart the function here with angr,
already giving a state with two symbolic digits.

31
00:02:50,100 --> 00:02:52,088
So, that's what we like to do.

32
00:02:53,087 --> 00:03:00,493
But we start the call
just after a scanf,

33
00:03:01,588 --> 00:03:07,839
and our target is
as always to return,

34
00:03:09,788 --> 00:03:17,839
but as we remember the values are
being stored by scanf on the stack.

35
00:03:18,339 --> 00:03:21,841
So, we don't want to destroy the
stack before we get the flag.

36
00:03:23,158 --> 00:03:33,837
That's why we just want to stop angr just
right before it destroys our stack frame,

37
00:03:34,839 --> 00:03:41,323
and this would be here.

38
00:03:42,584 --> 00:03:45,090
at 0x16cc,

39
00:03:49,086 --> 00:03:53,335
and now, we'll create blank state,

40
00:03:56,085 --> 00:04:03,945
because we don't have any information
besides what we guess to be two digits.

41
00:04:03,970 --> 00:04:15,892
So, we have a blank state that is starting
right after the scanf and with the Lazy Solver options off,

42
00:04:16,585 --> 00:04:20,723
and we need to push the
symbolic values into the stack.

43
00:04:22,586 --> 00:04:25,879
The same way that scanf would do.

44
00:04:25,904 --> 00:04:28,086
We are going to do it ourselves.

45
00:04:28,900 --> 00:04:37,586
So, we can use claripy,
bitvector symbolic.

46
00:04:37,800 --> 00:04:55,086
We can give the name we want like
phase_3_flag and bitvectors 64-bit,

47
00:04:55,585 --> 00:04:59,682
because of architecture of the binary.

48
00:04:59,707 --> 00:05:01,835
We can have it like this,

49
00:05:03,244 --> 00:05:12,771
or we can create it in a separate
variable like this, for example,

50
00:05:13,339 --> 00:05:17,086
and have it separated.

51
00:05:17,835 --> 00:05:22,086
You can add and parse this
variable to your function,

52
00:05:22,400 --> 00:05:29,035
the way it's easier to
read for you is to go

53
00:05:31,335 --> 00:05:33,085
and then, start the simulation,

54
00:05:33,584 --> 00:05:39,836
with this state that it was blank with the
start address right after the scanf.

55
00:05:41,087 --> 00:05:49,335
And the only value that it has is
bitvector of 64-bit and the stack.

56
00:05:49,600 --> 00:05:50,836
And that's all we have.

57
00:05:52,081 --> 00:05:54,086
We'll start the
simulation with the state,

58
00:05:55,336 --> 00:05:58,586
and as we know from the other levels,

59
00:05:58,835 --> 00:06:02,835
we have defined to
find it to the target

60
00:06:03,020 --> 00:06:12,085
that is just before the stack frame gets
destroyed for returning to the main function,

61
00:06:12,300 --> 00:06:25,336
We'll avoid the bomb and
let angr do its job.

62
00:06:27,085 --> 00:06:28,836
That was a typo there.

63
00:06:32,335 --> 00:06:34,336
Now, we'll wait.

64
00:06:34,774 --> 00:06:38,161
Angr found one solution.

65
00:06:38,186 --> 00:06:44,611
As you may remember from
looking into the binary

66
00:06:44,636 --> 00:06:50,669
before the manipulations
happen in 32-bits,

67
00:06:51,170 --> 00:06:55,418
but we gave the value as
a 64-bit in the stack.

68
00:06:56,100 --> 00:07:05,419
So, we just need to separate the low
and high values that we had on the stack.

69
00:07:08,435 --> 00:07:12,681
We can see that we have
the first one is zero

70
00:07:12,706 --> 00:07:23,932
and the second digit that is expecting
602 , we can store it

71
00:07:24,935 --> 00:07:25,936
and running it.

72
00:07:26,100 --> 00:07:30,936
we see the phase one, phase
two and phase three.

73
00:07:33,935 --> 00:07:39,685
As a homework, please implement
a loop adding constraints,

74
00:07:39,936 --> 00:07:41,984
so you can get all the possible flags.

75
00:07:42,685 --> 00:07:44,436
If you solve this level

76
00:07:44,825 --> 00:07:49,185
before you may remember that there
are more than one possibility.

77
00:07:49,935 --> 00:07:53,186
Just have a look again
into this switch.

78
00:07:53,944 --> 00:07:54,686
structure

79
00:07:56,435 --> 00:08:03,327
or simply leave it open to angr to
give you all the possible solutions

80
00:08:05,435 --> 00:08:07,685
with an while loop or something similar.

81
00:08:08,479 --> 00:08:11,039
So, that's it.
Phase three.

